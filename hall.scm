(hall-description
  (name "zip")
  (prefix "guile")
  (version "0.1")
  (author "AitorATuin")
  (copyright (2020))
  (synopsis "")
  (description "")
  (home-page "")
  (license gpl3+)
  (dependencies `(("libzip" ,libzip)
		  ("zlib" ,zlib)))
  (files (libraries
          ((scheme-file "zip")
	   (directory
	    "zip"
	    (scheme-file "lzip")
	    (scheme-file "utils")
	    (scheme-file "zlib")
	    (directory
	     "ffi"
	     (scheme-file "lzip")
	     (scheme-file "zlib")))))
         (tests ((directory "tests" ())))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "zip")))))
         (infrastructure
           ((scheme-file "guix")
            (text-file ".gitignore")
            (scheme-file "hall")))))
