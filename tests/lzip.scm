(define-module (tests lzip)
  #:use-module (zip lzip)
  #:use-module (zip utils)
  #:use-module (system foreign)
  #:use-module (srfi srfi-64))

(define zf #nil)
(define err (make-int32))

(define (open-zf file)
  (set! zf (lzip/open file 0 (err '&))))

(define* (cleanup #:key discard)
  (err 0)
  (when (pointer? zf)
    (if discard
	(lzip/discard zf)
	(lzip/close zf))))

(test-begin "lzip")

(test-equal "libzip version" "1.7.3" (lzip/version))

(test-begin "open/close")

(test-group-with-cleanup "zip file doesn't exists"
  (begin
    (open-zf "do-not-exists")
    ;; (set! zf (lzip/open "do-not-exists" 0 (err '&)))
    (test-assert "lzip/open returns null-pointer" (null-pointer? zf))
    (test-eq "lzip/open gives error is 9" (err) 9))
  (cleanup))

(test-group-with-cleanup "zip file does exist"
  (begin
    (open-zf "tests/files/test.zip")
    (test-assert "lzip/open returns a valid pointer" (pointer? zf))
    (test-eq "lzip/open gives no error" 0 (err))
    (test-assert "lzip/close can close the file" (lzip/close zf))
    (set! zf %null-pointer))
  (cleanup))

(test-end "open/close")

(test-begin "zip_t struct")

(test-group-with-cleanup "basic operations with zip_t"
  (begin
    (open-zf "tests/files/test.zip")
    (test-eq "lzip/get-num-entries"
      (lzip/get-num-entries zf 0) 3)
    (test-equal "lzip/get-name"
      '("one" "two" "three") (map (lambda (n) (lzip/get-name zf n 0)) (iota 3 0)))
    (test-assert "lzip/get-file-name with wrong index"
      (null? (lzip/get-name zf 4 0)))
    (test-equal "lzip/get-error contains the zip-error"
      (make-zip-error 18 0 #nil) ((lzip/get-error zf)))
    (test-equal "lzip/strerror contains a string with the error"
      "Invalid argument" (lzip/strerror zf))
    (test-equal "lzip/error-strerror returns the same error string"
      "Invalid argument" (lzip/error-strerror ((lzip/get-error zf) '&)))
    (test-equal "empty lzip/file-get-comment"
      '("" "" "") (map (lambda (n) (lzip/file-get-comment zf n 0 0)) (iota 3 0)))
    (test-equal "lzip/name-locate can find files"
      '(0 1 2) (map (lambda (n) (lzip/name-locate zf n 0)) '("one" "two" "three")))
    (test-equal "lzip/name-locate returns -1 if file is not present in zip"
      -1 (lzip/name-locate zf "wtf" 0))
    (test-equal "lzip/get-archive-comment"
      "" (lzip/get-archive-comment zf %null-pointer 0))
    (lzip/set-archive-comment zf "This is a test comment")
    (test-equal "lzip/set-archive-comment"
      "This is a test comment" (lzip/get-archive-comment zf %null-pointer 0)))
  (cleanup #:discard #t))

(test-end "zip_t struct")

(test-end "lzip")
