(define-module (zip)
  #:use-module (zip zlib)
  #:use-module (zip lzip))

(define-syntax re-export-modules
  (syntax-rules ()
    ((_ (mod ...) ...)
     (begin
       (module-use! (module-public-interface (current-module))
		    (resolve-interface '(mod ...)))
       ...))))

(re-export-modules (zip lzip)
		   (zip zlib))
