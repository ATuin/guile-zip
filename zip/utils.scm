(define-module (zip utils)
  #:use-module (system foreign)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 hash-table)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs enums)
  #:export (make-int8
	    make-int32
	    make-int64
	    c-struct-offsets
	    c-struct-get
	    c-struct-set!
	    pointer->maybe-string
	    bytevector-offset
	    define-enum
	    define-flag))

(define* (make-c-type type min max set ref #:optional value)
  "Creates a new c type."
  (define (overflow? v)
    (or (> v max) (< v min)))
  (define (raise-overflow-exception v)
    (raise-exception (make-exception
		      (make-programming-error)
		      (make-exception-with-message "Type overflow")
		      (make-exception-with-irritants (list type v)))
		     #:continuable? #f))
  (define (ref-bv bv endianness)
    (if endianness
	(ref bv 0 endianness)
	(ref bv 0)))
  (define (set-bv! bv v endianness)
    ;; (when (overflow? v)
    ;;   (raise-overflow-exception v))
    (begin
      (if endianness
	  (set bv 0 v endianness)
	  (set bv 0 v))
      (ref-bv bv endianness)))
  (let* ((size (sizeof type))
	 (endianness (and (> (sizeof type) 1) (native-endianness)))
	 (bv (make-bytevector size 0))
	 (v (or value 0)))
    (when value
      (set-bv! bv v endianness))
    (lambda* (#:optional op args)
      (match `(,op ,args)
	((#f #f) (ref-bv bv endianness))
	(('& #f) (bytevector->pointer bv))
	((v #f) (set-bv! bv v endianness))))))

(define* (make-int8 #:optional v)
  "Make an int8 type"
  (make-c-type int8 -128 128 bytevector-s8-set! bytevector-s8-ref v))

(define* (make-int32 #:optional v)
  "Make an int32 type"
  (make-c-type int32 -2147483648 2147483648 bytevector-s32-set! bytevector-s32-ref v))

;;; define-enum / define-flags macros
;;
;; Those macros allows to define enums and flags
;;
;; An *enum* is defined like this:
;;    ------
;;
;; `(define-enum my-enum ((A 1) (B 2) ...))`
;;
;; it will define the following functions:
;;  - int->my-enum int :: converts an int into a symbol in enum
;;  - my-enum->int symbol :: converts a symbol in enum into an int
;;  - my-enum? symbol :: predicate to check if symbol is in enum
;;  - my-enum-subseb? list-symbols :: returns a predicate that checks if a symbol
;;     is in the list-of-sybols (which must be members of enum)
;;
;; A *flag* is defined like this:
;;   -------
;;
;; `(define-flag my-flag ((A 1) (B 2) ...))`
;;
;; It will define the same functions as *enum* with an extra function:
;;  - my-flag-or symbols :: It will return the OR of the symbols

(define (names stx n flags)
  (let* ((name-s (symbol->string n))
	 (xs (list
	      (datum->syntax stx (string->symbol (string-append name-s "?")))
	      (datum->syntax stx (string->symbol (string-append name-s "-subset?")))
	      (datum->syntax stx (string->symbol (string-append name-s "->int")))
	      (datum->syntax stx (string->symbol (string-append "int->" name-s)))
	      (datum->syntax stx (string->symbol (string-append name-s "-or"))))))
    (if flags xs (take xs 4))))

;; TODO: Improve the error throwing an exception
(define (fns st str flags)
  (define (error-s s)
    (error (string-append "Invalid symbol: " (symbol->string s))))
  (define (error-i i)
    (error (string-append "No symbol for " (number->string i))))
  (let ((fns (list
	      (lambda (s) (not (nil? (hash-ref st s #f))))
	      (lambda (ss)
		(lambda (s)
		  (let ((m (member s ss)))
		    (and m (not (nil? (hash-ref st s #f)))))))
	      (lambda (s) (or (hash-ref st s) (error-s s)))
	      (lambda (i) (or (hash-ref str i) (error-i i)))
	      (lambda (xs)
		(apply logior (map (lambda (s)
				     (or (hash-ref st s) (error-s s)))
				   xs))))))
    (if flags fns (take fns 4))))

(define-syntax-rule (define-enum-or-flag name ((s v) ...) (n ...) flags)
  (define-values (n ...)
    (let ((st (alist->hash-table `((s . v) ...)))
	  (str (alist->hash-table (map (lambda (p)
					 (cons (cdr p) (car p)))
				       `((s . v) ...)))))
      (apply values (fns st str flags)))))

(define-syntax define-enum
  (lambda (x)
    (syntax-case x ()
      ((_ name ((s v) ...))
       (with-syntax (((n ...) (names x (syntax->datum #'name) #f)))
	 #`(define-enum-or-flag name ((s v) ...) (n ...) #f))))))

(define-syntax define-flag
  (lambda (x)
    (syntax-case x ()
      ((_ name ((s v) ...))
       (with-syntax (((n ...) (names x (syntax->datum #'name) #t)))
	 #`(define-enum-or-flag name ((s v) ...) (n ...) #t))))))

;; Reuse some code from (system foreign)
(define align (@@ (system foreign) align))
(define *readers* (@@ (system foreign) *readers*))
(define *writers* (@@ (system foreign) *writers*))

(define (c-struct-offsets types)
  "Procedure that return a list of cons (type . offset) for a c struct \
represented by types"
  (let lp ((offset 0) (types types) (offsets '()))
    (let* ((type (and (pair? types) (car types)))
           (offset (and type (align offset (alignof type)))))
      (if offset
	  (lp (+ offset (sizeof type)) (cdr types) (cons `(,type . ,offset) offsets))
	  (list->vector (reverse offsets))))))

(define (c-struct-get struct offsets idx)
  "Get field at position idx in the struct.
Offsets is used to know the position of field."
  (let ((offset (vector-ref offsets idx)))
    ((assv-ref *readers* (car offset)) struct (cdr offset))))

(define (c-struct-set! struct offsets idx value)
  "Set field at position idx in the struct with value.
Offsets is used to know the position of field."
  (let ((offset (vector-ref offsets idx)))
    ((assv-ref *writers* (car offset)) struct (cdr offset) value)))

(define* (make-int64 #:optional v)
  "Make an int32 type"
  (make-c-type int64 -9223372036854775808 9223372036854775808 bytevector-s64-set!
	       bytevector-s64-ref v))

(define (pointer->maybe-string p)
  "Returns the string in the pointer p."
  (if (null-pointer? p) #nil (pointer->string p)))

(define* (bytevector-offset bv offset #:optional len)
  (let ((len (or len (bytevector-length bv))))
    (pointer->bytevector (bytevector->pointer bv offset) (- len offset))))
