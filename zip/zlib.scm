(define-module (zip zlib)
  #:use-module (zip ffi zlib)
  #:use-module (zip utils)
  #:use-module (system foreign)
  #:use-module (ice-9 match)
  #:use-module (ice-9 binary-ports)
  #:use-module (rnrs bytevectors)
  #:export (inflate
	    inflate->bytevector
	    deflate
	    deflate->bytevector))

(define (get-bytevector-some-or-0! port buf buf-len)
  (let ((n (get-bytevector-some! port buf 0 buf-len)))
    (or (and (eof-object? n) 0) n)))

(define (lookahead-eof? port)
  (eof-object? (lookahead-u8 port)))

(define* (inflate port-in port-out #:key buf-len (raw-inflate #f))
  (unless buf-len
    (set! buf-len 16384))
  (define buf-in (make-bytevector buf-len))
  (define buf-out (make-bytevector buf-len))
  (define flush 'Z-NO-FLUSH)
  (define ret #f)
  (define (loop-in zstream)
    (zstream-avail-out! zstream buf-len)
    (zstream-next-out! zstream (bytevector->pointer buf-out))
    (set! ret (zlib/inflate zstream flush))
    (put-bytevector port-out buf-out 0 (- buf-len (zstream-avail-out zstream)))
    (when (and (eq? 'Z-OK ret) (zero? (zstream-avail-out zstream)))
      (loop-in zstream)))
  (define (loop-out zstream)
    (match ret
      ('Z-STREAM-END #t)
      ('Z-OK
       (zstream-avail-in! zstream (get-bytevector-some-or-0! port-in buf-in buf-len))
       (zstream-next-in! zstream (bytevector->pointer buf-in))
       (loop-in zstream)
       (loop-out zstream))
      (err (zlib/inflate-end zstream)
	   (error (format #f "Error: inflate: ~a" err)))))
  (let ((zstream (make-zstream)))
    (set! ret (zlib/inflate-init zstream #:raw-inflate raw-inflate))
    (loop-out zstream)))

(define* (inflate->bytevector port-or-bv #:key (buf-len Z-BUF-LEN) raw-inflate
			      (offset 0) len)
  (define port-in (cond
		   ((bytevector? port-or-bv)
		    (open-bytevector-input-port
		     (bytevector-offset port-or-bv offset len)))
		   ((port? port-or-bv) port-or-bv)
		   (else (error "Invalid argument"))))
  (call-with-values
      (lambda () (open-bytevector-output-port))
    (lambda (port-out get-bytevector-out)
      (inflate port-in port-out #:buf-len buf-len #:raw-inflate raw-inflate)
      (close-port port-out)
      (close-port port-in)
      (get-bytevector-out))))

(define* (deflate port-in port-out
	   #:key (buf-len Z-BUF-LEN) (level Z-DEFAULT-COMPRESSION))
  (define buf-in (make-bytevector buf-len))
  (define buf-out (make-bytevector buf-len))
  (define flush 'Z-NO-FLUSH)
  (define z-return-ok? (z-return-subset? '(Z-OK Z-STREAM-END)))
  (define ret #f)
  (define (loop-in zstream)
    (zstream-avail-out! zstream buf-len)
    (zstream-next-out! zstream (bytevector->pointer buf-out))
    (set! ret (zlib/deflate zstream flush))
    (put-bytevector port-out buf-out 0 (- buf-len (zstream-avail-out zstream)))
    (when (and (eq? 'Z-OK ret) (zero? (zstream-avail-out zstream)))
      (loop-in zstream)))
  (define (loop-out zstream)
    (cond
     ((z-return-ok? ret)
      (match flush
	('Z-FINISH (zlib/deflate-end zstream))
	(_
	 (zstream-avail-in! zstream (get-bytevector-some-or-0! port-in buf-in
							       buf-len))
	 (when (lookahead-eof? port-in)
	   (set! flush 'Z-FINISH))
	 (zstream-next-in! zstream (bytevector->pointer buf-in))
	 (loop-in zstream)
	 (loop-out zstream))))
     (else (zlib/deflate-end zstream)
	   (error (format #f "Error: deflate: ~a" ret)))))
  (let ((zstream (make-zstream)))
    (set! ret (zlib/deflate-init zstream level))
    (loop-out zstream)))

(define* (deflate->bytevector port
	   #:key (buf-len Z-BUF-LEN) (level Z-DEFAULT-COMPRESSION))
  (call-with-values
      (lambda () (open-bytevector-output-port))
    (lambda (port-out get-bytevector-out)
      (deflate port port-out #:buf-len buf-len #:level level)
      (get-bytevector-out))))
