(define-module (zip ffi zlib)
  #:use-module (zip utils)
  #:use-module (system foreign)
  #:use-module (rnrs enums)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (ice-9 binary-ports)
  #:use-module (srfi srfi-9)
  #:export (z-return?
	    int->z-return
	    z-return->int
	    z-return-subset?
	    z-flush?
	    int->z-flush
	    z-flush->int
	    z-flush-subset?
	    Z-NULL
	    Z-BUF-LEN
	    Z-DEFAULT-COMPRESSION
	    make-zstream
	    zstream->pointer
	    zstream-next-in
	    zstream-next-in!
	    zstream-avail-in
	    zstream-total-in
	    zstream-avail-in!
	    zstream-next-out
	    zstream-next-out!
	    zstream-avail-out
	    zstream-avail-out!
	    zstream-total-out
	    zlib/version
	    zlib/deflate
	    zlib/deflate-init
	    zlib/deflate-end
	    zlib/inflate
	    zlib/inflate-init
	    zlib/inflate-end))

(define zlib (dynamic-link "libz"))
(define (zlib-func return name args)
  (pointer->procedure return (dynamic-func name zlib) args))

(define-enum z-flush
  ((Z-NO-FLUSH 0) (Z-PARTIAL-FLUSH 1) (Z-SYNC-FLUSH 2) (Z-FULL-FLUSH 3)
   (Z-FINISH 4) (Z-BLOCK 5) (Z-TREES 6)))

(define-enum z-return
  ((Z-VERSION-ERROR -6) (Z-BUF-ERROR -5) (Z-MEM-ERROR -4) (Z-DATA-ERROR -3)
   (Z-STREAM-ERROR -2) (Z-ERRNO -1) (Z-OK 0) (Z-STREAM-END 1) (Z-NEED-DICT 2)))

;; some constants
(define Z-NULL 0)
(define Z-BUF-LEN 16348)
(define Z-DEFAULT-COMPRESSION -1)

;; zstream type used internally by zlib
(define-record-type <zstream>
  (new-zstream offsets bv)
  zstream?
  (offsets zstream-offsets)
  (bv zstream->bytevector))

(define (zstream->pointer zstream)
  (bytevector->pointer (zstream->bytevector zstream)))

(define (zstream-next-in! zstream value)
  (c-struct-set! (zstream->bytevector zstream) (zstream-offsets zstream) 0 value))

(define (zstream-next-in zstream)
  (c-struct-get (zstream->bytevector zstream) (zstream-offsets zstream) 0))

(define (zstream-avail-in! zstream value)
  (c-struct-set! (zstream->bytevector zstream) (zstream-offsets zstream) 1 value))

(define (zstream-avail-in zstream)
  (c-struct-get (zstream->bytevector zstream) (zstream-offsets zstream) 1))

(define (zstream-total-in zstream)
  (c-struct-get (zstream->bytevector zstream) (zstream-offsets zstream) 2))

(define (zstream-next-out! zstream value)
  (c-struct-set! (zstream->bytevector zstream) (zstream-offsets zstream) 3 value))

(define (zstream-next-out zstream)
  (c-struct-get (zstream->bytevector zstream) (zstream-offsets zstream) 3))

(define (zstream-avail-out! zstream value)
  (c-struct-set! (zstream->bytevector zstream) (zstream-offsets zstream) 4 value))

(define (zstream-avail-out zstream)
  (c-struct-get (zstream->bytevector zstream) (zstream-offsets zstream) 4))

(define (zstream-total-out zstream)
  (c-struct-get (zstream->bytevector zstream) (zstream-offsets zstream) 5))

(define (make-zstream)
  "Creates a new zstream."
  (let* ((zstream-t (list '* unsigned-int unsigned-long '* unsigned-int unsigned-long '*
			  '* '* '* '* int unsigned-long unsigned-long))
	 (zstream* (make-c-struct zstream-t (list %null-pointer Z-NULL Z-NULL
						  %null-pointer Z-NULL Z-NULL
						  %null-pointer %null-pointer
						  %null-pointer %null-pointer
						  %null-pointer Z-NULL Z-NULL Z-NULL))))
    (new-zstream (c-struct-offsets zstream-t)
		 (pointer->bytevector zstream* (sizeof zstream-t)))))

(define (zlib/version)
  (pointer->string ((zlib-func '* "zlibVersion" '()))))

(define (zlib/deflate zstream flush)
  (int->z-return ((zlib-func int "deflate" (list '* int)) (zstream->pointer zstream)
		    (z-flush->int flush))))

(define (zlib/deflate-init zstream level)
  (let ((proc (zlib-func int "deflateInit_" (list '* int '* int))))
    (int->z-return (proc (zstream->pointer zstream) level
			   (string->pointer (zlib/version))
			   (sizeof (map car (vector->list
					     (zstream-offsets zstream))))))))

(define (zlib/deflate-end zstream)
 ((zlib-func void "deflateEnd" (list '*)) (zstream->pointer zstream)))

(define (zlib/inflate zstream flush)
  (int->z-return ((zlib-func int "inflate" (list '* int)) (zstream->pointer zstream)
		    (z-flush->int flush))))

(define* (zlib/inflate-init zstream #:key (raw-inflate #f))
  (let ((proc1 (zlib-func int "inflateInit_" (list '* '* int)))
	(proc2 (zlib-func int "inflateInit2_" (list '* int '* int)))
	(zstream* (zstream->pointer zstream))
	(version (string->pointer (zlib/version)))
	(size (sizeof (map car (vector->list (zstream-offsets zstream))))))
    (int->z-return (if raw-inflate
			 (proc2 zstream* -15 version size)
			 (proc1 zstream* version size)))))

(define (zlib/inflate-end zstream)
  ((zlib-func void "inflateEnd" (list '*)) (zstream->pointer zstream)))
