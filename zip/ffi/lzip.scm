(define-module (zip ffi lzip)
  #:use-module (zip utils)
  #:use-module (system foreign)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:export (;; enums and flags
	    zip-comp-method->int
	    zip-comp-method?
	    zip-comp-method-subset?
	    int->zip-comp-method
	    zip-comp-level->int
	    zip-comp-level?
	    int->zip-comp-level
	    int->zip-open-flag
	    zip-open-flag->int
	    zip-open-flag?
	    int->zip-file-flag
	    zip-file-flag->int
	    zip-file-flag?
	    int->zip-archive-flag
	    zip-archive-flag->int
	    zip-archive-flag?
	    ;; misc procedures
	    lzip/compression-method-supported
	    lzip/encryption-method-supported
	    lzip/version
	    ;; error procedures
	    lzip/get-error
	    lzip/strerror
	    lzip/file-strerror
	    lzip/error-strerror
	    ;; archive procedures
	    lzip/open
	    lzip/fdopen
	    lzip/close
	    lzip/discard
	    lzip/get-num-entries
	    lzip/get-archive-comment
	    lzip/set-archive-comment
	    lzip/get-archive-flag
	    lzip/set-archive-flag
	    lzip/unchange-all
	    lzip/unchange-archive
	    lzip/set-default-password
	    ;; file procedures
	    lzip/stat-index
	    lzip/stat
	    lzip/delete
	    lzip/rename
	    lzip/file-rename
	    lzip/file-get-comment
	    lzip/file-set-comment
	    lzip/name-locate
	    lzip/get-name
	    lzip/unchange
	    lzip/fclose
	    lzip/fopen
	    lzip/fopen-index
	    lzip/fopen-encrypted
	    lzip/fopen-index-encrypted
	    lzip/fseek
	    lzip/ftell
	    lzip/fread
	    lzip/file-set-dostime
	    lzip/file-get-external-attributes
	    lzip/file-set-external-attributes
	    lzip/file-set-encryption
	    lzip/file-set-compression
	    lzip/dir-add
	    lzip/file-add
	    lzip/file-replace
	    lzip/file-extra-field-get
	    lzip/file-extra-field-get-by-id
	    lzip/file-extra-field-delete
	    lzip/file-extra-field-delete-by-id
	    lzip/file-extra-field-set
	    lzip/file-extra-fields-count
	    lzip/file-extra-fields-count-by-id
	    ;; source
	    lzip/source-buffer
	    lzip/source-buffer-create
	    lzip/source-close
	    lzip/source-open
	    lzip/source-read
	    lzip/source-error
	    lzip/source-stat
	    ;; records
	    make-zip-stat
	    zip-stat?
	    zip-stat-name
	    zip-stat-index
	    zip-stat-comp-method
	    zip-stat-valid
	    zip-stat-size
	    zip-stat-mtime
	    zip-stat-comp-size
	    zip-stat-crc
	    zip-stat-encryption-method
	    zip-stat-flags
	    zip-error
	    zip-error?
	    zip-error-str
	    zip-error-sys-err
	    zip-error-zip-err))

(define lzip (dynamic-link "libzip"))

(define zip-flags-t uint32)

(define-enum zip-comp-method
  ((CM-DEFAULT -1) (CM-STORE 0) (CM-SHRINK 1) (CM-REDUCE-1 2) (CM-REDUCE-2 3)
   (CM-REDUCE-3 4) (CM-REDUCE-4 5) (CM-IMPLODE 6) (CM-DEFLATE 8) (CM-DEFLATE-64 9)
   (CM-PKWARE-IMPLODE 10) (CM-BZIP2 12) (CM-LZMA 14) (CM-TERSE 18) (CM-LZ77 19)
   (CM-LZMA2 33) (CM-XZ 95) (CM-JPEG 96) (CM-WAVPACK 97) (CM-PPMD 98)))

(define-enum zip-comp-level
  ((CM-LEVEL1 1) (CM-LEVEL2 2) (CM-LEVEL3 3) (CM-LEVEL4 4) (CM-LEVEL5 5) (CM-LEVEL6 6)
   (CM-LEVEL7 7) (CM-LEVEL8 8) (CM-LEVEL9 9) (CM-LEVEL-DEFAULT 0)))

(define-flag zip-open-flag
  ((ZIP-CREATE 1) (ZIP-EXCL 2) (ZIP-CHECKCONS 4) (ZIP-TRUNCATE 8) (ZIP-RDONLY 16)))

(define-flag zip-file-flag
  ((FL-NOCASE 1) (FL-NODIR 2) (FL-COMPRESSED 4) (FL-UNCHANGED 8) (FL-RECOMPRESS 16)
   (FL-ENCRYPTED 32) (FL-ENC-GUESS 0) (FL-ENC-RAW 64) (FL-ENC-STRICT 128)
   (FL-LOCAL 256) (FL-CENTRAL 512) (FL-ENC-UTF-8 2048) (FL-ENC-CP437 4096)
   (FL-OVERWRITE 8192)))

(define-flag zip-archive-flag
  ((AFL_RDONLY 2)))

;; TODO: Make a macro
(define-immutable-record-type <zip-stat>
  (new-zip-stat valid name index size comp-size mtime crc comp-method
		encryption-method flags)
  zip-stat?
  (valid zip-stat-valid) ;; which fields have valid values
  (name zip-stat-name) ;; name of the file
  (index zip-stat-index) ;; index within the archive
  (size zip-stat-size) ;; zize of file (uncompressed)
  (comp-size zip-stat-comp-size) ;; size of file (compressed)
  (mtime zip-stat-mtime) ;; modification time
  (crc zip-stat-crc) ;; crc of file data
  (comp-method zip-stat-comp-method) ;; compression method used
  (encryption-method zip-stat-encryption-method) ;; encryption method used
  (flags zip-stat-flags)) ;; reserved for future use 

(define (make-zip-stat)
  (let* ((zip-stat-t (list uint64 '* uint64 uint64 uint64 long uint32 uint16 uint16
			   uint32))
	 (zip-stat* (make-c-struct zip-stat-t (list 0 %null-pointer 0 0 0 0 0 0 0 0))))
    (lambda* (#:optional op)
      (match op 
	(#f (match-let (((a s b c d e f g h i) (parse-c-struct zip-stat* zip-stat-t)))
	      (new-zip-stat a (pointer->maybe-string s) b c d e f g h i)))
	('& zip-stat*)
	(_ (error "Unknown operator on c struct"))))))

;; zip_error struct

(define-immutable-record-type <zip-error>
  (make-zip-error zip-err sys-err str)
  zip-error?
  (zip-err zip-error-zip-err) ;; libzip error code
  (sys-err zip-error-sys-err) ;; copy of errno or zlib error code
  (str zip-error-str)) ;; string representation

(define* (zip-error #:optional ptr)
  (let* ((zip-error-t (list int int '*))
	 (zip-error* (or (and ptr (not (null-pointer? ptr)))
			 (make-c-struct zip-error-t (list 0 0 %null-pointer)))))
    (lambda* (#:optional op)
      (match op
	(#f (match-let (((a b c) (parse-c-struct zip-error* zip-error-t)))
	      (make-zip-error a b (pointer->maybe-string c))))
	('& zip-error*)
	(_ (error "Unknown operator on c struct"))))))

;; (define-c-struct <zip-error>
;;   ((zip-err int 0)
;;    (sys-err int 0)
;;    (str '* %null-pointer)))

;; TODO: Make a macro
(define (lzip-func return name args)
  (pointer->procedure return (dynamic-func name lzip) args))

;; int zip_compression_method_supported(zip_int32_t method, int compress)
(define lzip/compression-method-supported
  (lzip-func int "zip_compression_method_supported" (list uint32 int)))

;; int zip_encryption_method_supported(zip_int16_t method, int encrypt)
(define lzip/encryption-method-supported
  (lzip-func int "zip_encryption_method_supported" (list uint16 int)))

;;; 
;; const char * zip_libzip_version(void)
(define (lzip/version)
  (let ((f (lzip-func '* "zip_libzip_version" '())))
    (pointer->maybe-string (f))))

;;; archive functions

;; zip_t * zip_open(const char *path, int flags, int *errorp)
(define (lzip/open file flags errorp)
  (let ((f (lzip-func '* "zip_open" (list '* int '*))))
    (f (string->pointer file) (zip-open-flag-or flags) errorp)))

;; zip_t * zip_fdopen(int fd, int flags, int *errorp)
(define (lzip/fdopen fd flags errorp)
  (let ((f (lzip-func '* "zip_fdopen" (list int int '*))))
    (f fd (zip-open-flag-or flags) errorp)))

;; int zip_close(zip_t *archive)
(define lzip/close (lzip-func int "zip_close" (list '*)))

;; void zip_discard(zip_t *archive)
(define lzip/discard (lzip-func int "zip_discard" (list '*)))

;; const char * zip_get_archive_comment(zip_t *archive, int *lenp, zip_flags_t flags)
(define (lzip/get-archive-comment archive* lenp* flags)
  (let ((f (lzip-func '* "zip_get_archive_comment" (list '* '* zip-flags-t))))
    (pointer->maybe-string (f archive* lenp* (zip-file-flag-or flags)))))

;; int zip_set_archive_comment(zip_t *archive, const char *comment, zip_uint16_t len)
(define lzip/set-archive-comment
  (let ((f (lzip-func int "zip_set_archive_comment" (list '* '* uint16))))
    (lambda (archive* comment)
      (f archive* (string->pointer comment) (string-length comment)))))

;; int zip_get_archive_flag(zip_t *archive, zip_flags_t flag, zip_flags_t flags)
(define (lzip/get-archive-flag archive* flag flags)
  (let ((f (lzip-func int "zip_get_archive_flag" (list '* zip-flags-t zip-flags-t))))
    (f archive* (zip-archive-flag->int flag) (zip-archive-flag-or flags))))

;; int zip_set_archive_flag(zip_t *archive, zip_flags_t flag, int value)
(define (lzip/set-archive-flag archive* flag value)
  (let ((f (lzip-func int "zip_set_archive_flag" (list '* zip-flags-t int))))
    (f archive* (zip-archive-flag->int flag) value)))

;; zip_int64_t zip_get_num_entries(zip_t *archive, zip_flags_t flags);
(define (lzip/get-num-entries archive* flags)
  (let ((f (lzip-func int64 "zip_get_num_entries" (list '* zip-flags-t))))
    (f archive* (zip-file-flag-or flags))))

;; int zip_unchange_all(zip_t *archive)
(define lzip/unchange-all (lzip-func int "zip_unchange_all" (list '*)))

;; int zip_unchange_archive(zip_t *archive)
(define lzip/unchange-archive (lzip-func int "zip_unchange_archive" (list '*)))

;; int zip_set_default_password(zip_t *archive, const char *password)
(define lzip/set-default-password
  (lzip-func int "zip_set_default_password" (list '* '*)))

;; file functions
;; ==============
;; functions that operates on files in the zip archive

;; const char * zip_file_get_comment(zip_t *archive, zip_uint64_t index,
;;                                   zip_uint32_t *lenp, zip_flags_t flags)
(define (lzip/file-get-comment archive* idx lenp flags)
  (let ((f (lzip-func '* "zip_file_get_comment" (list '* uint64 uint32 zip-flags-t))))
    (pointer->maybe-string (f archive* idx lenp (zip-file-flag-or flags)))))

;; int zip_file_set_comment(zip_t *archive, zip_uint64_t index, const char *comment,
;;                          zip_uint16_t len, zip_flags_t flags)
(define (lzip/file-set-comment archive* idx comment len flags)
  (let ((f (lzip-func '* "zip_file_set_comment"
		      (list '* uint64 '* uint16 zip-flags-t))))
    (f archive* idx (string->pointer comment) len
       (zip-file-flag-or flags))))

;; int zip_file_set_dostime(zip_t *archive, zip_uint64_t index, zip_uint16_t dostime,
;;                          zip_uint16_t dosdate, zip_flags_t flags)
(define lzip/file-set-dostime
  (lzip-func int "zip_file_set_dostime" (list '* uint64 uint16 uint16 zip-flags-t)))

;; int zip_file_get_external_attributes(zip_t *archive, zip_uint64_t index,
;;                                      zip_flags_t flags, zip_uint8_t *opsys,
;;                                      zip_uint32_t *attributes)
(define (lzip/file-get-external-attributes archive* idx flags opsys attrs)
  (let ((f (lzip-func int "zip_file_get_external_attributes"
		      (list '* uint64 zip-flags-t '* '*))))
    (f archive* idx (zip-file-flag-or flags) opsys attrs)))

;; int zip_file_set_external_attributes(zip_t *archive, zip_uint64_t index,
;;                                      zip_flags_t flags, zip_uint8_t opsys,
;;                                      zip_uint32_t attributes)
(define lzip/file-set-external-attributes
  (lzip-func int "zip_file_set_external_attributes"
	     (list '* uint64 zip-flags-t '* '*)))

;; int zip_file_set_encryption(zip_t *archive, zip_uint64_t index, zip_uint16_t method,
;;                             const char *password)
(define lzip/file-set-encryption
  (let ((f (lzip-func int "zip_file_set_encryption" (list '* uint64 uint16 '*))))
    (lambda (archive* index method password)
      (f archive* index method (string->pointer password)))))

;; int zip_set_file_compression(zip_t *archive, zip_uint64_t index, zip_int32_t comp,
;;                              zip_uint32_t comp_flags)
(define (lzip/file-set-compression archive idx cm cm-flags)
  (let ((f (lzip-func int "zip_set_file_compression" (list '* uint64 int32 uint32))))
    (f archive idx (zip-comp-method->int cm) (zip-comp-level->int cm-flags))))

;; int zip_stat_index(zip_t *archive, zip_uint64_t index, zip_flags_t flags,
;;                    zip_stat_t *sb)
(define (lzip/stat-index archive* idx flags stat*)
  (let ((f (lzip-func int "zip_stat_index" (list '* uint64 zip-flags-t '*))))
    (f archive* idx (zip-file-flag-or flags) stat*)))

;; int zip_stat(zip_t *archive, const char *fname, zip_flags_t flags, zip_stat_t *sb)
(define (lzip/stat archive* fname flags stat*)
  (let ((f (lzip-func int "zip_stat" (list '* '* zip-flags-t '*))))
    (f archive* (string->pointer fname) (zip-file-flag-or flags) stat*)))

;; zip_int64_t zip_name_locate(zip_t *archive, const char *fname, zip_flags_t flags)
(define (lzip/name-locate archive fname flags)
  (let ((f (lzip-func uint64 "zip_name_locate" (list '* '* zip-flags-t))))
    ((make-int64 (f archive (string->pointer fname) (zip-file-flag-or flags))))))

;; const char * zip_get_name(zip_t *archive, zip_uint64_t index, zip_flags_t flags)
(define (lzip/get-name archive* idx flags)
  (let ((f (lzip-func '* "zip_get_name" (list '* uint64 zip-flags-t))))
    (pointer->maybe-string (f archive* idx (zip-file-flag-or flags)))))

;; int zip_delete(zip_t *archive, zip_uint64_t index)
(define lzip/delete (lzip-func int "zip_delete" (list '* uint64)))

;; int zip_rename(zip_t *archive, zip_uint64_t index, const char *name)
;; obsolete: use lzip/file-rename instead
(define lzip/rename
  (let ((f (lzip-func int "zip_rename" (list '* uint64 '*))))
    (lambda (archive* index name)
      (f archive* index (string->pointer name)))))

;; int zip_file_rename(zip_t *archive, zip_uint64_t index, const char *name,
;;                     zip_flags_t flags)
(define lzip/file-rename
  (let ((f (lzip-func int "zip_file_rename" (list '* uint64 '* zip-flags-t))))
    (lambda (archive* index name flags)
      (f archive* index (string->pointer name) flags))))

;; int zip_unchange(zip_t *archive, zip_uint64_t index)
(define lzip/unchange (lzip-func int "zip_unchange" (list '* uint64)))

;; zip_file_t * zip_fopen(zip_t *archive, const char *fname, zip_flags_t flags)
(define lzip/fopen
  (let ((f (lzip-func '* "zip_fopen" (list '* '* zip-flags-t))))
    (lambda (zip-t* fname flags)
      (f zip-t* (string->pointer fname) flags))))

;; zip_file_t * zip_fopen_index(zip_t *archive, zip_uint64_t index, zip_flags_t flags)
(define lzip/fopen-index (lzip-func '* "zip_fopen_index" (list '* uint64 zip-flags-t)))

;; zip_file_t * zip_fopen_encrypted(zip_t *archive, const char *fname,
;;                                  zip_flags_t flags, const char *password)
(define lzip/fopen-encrypted
  (lzip-func '* "zip_fopen_encrypted" (list '* '* zip-flags-t '*)))

;; zip_file_t * zip_fopen_index_encrypted(zip_t *archive, zip_uint64_t index,
;;                                        zip_flags_t flags, const char *password)
(define lzip/fopen-index-encrypted
  (lzip-func '* "zip_fopen_encrypted" (list '* uint64 zip-flags-t '*)))

;; int zip_fclose(zip_file_t *file)
(define lzip/fclose (lzip-func int "zip_fclose" (list '*)))

;; zip_int8_t zip_fseek(zip_file_t *file, zip_int64_t offset, int whence)
(define lzip/fseek (lzip-func int8 "zip_fseek" (list '* int64 int)))

;; zip_int64_t zip_ftell(zip_file_t *file)
(define lzip/ftell (lzip-func int64 "zip_ftell" (list '*)))

;; zip_int64_t zip_fread(zip_file_t *file, void *buf, zip_uint64_t nbytes)
(define lzip/fread (lzip-func int64 "zip_fread" (list '* '* uint64)))

;; zip_int64_t zip_dir_add(zip_t *archive, const char *name, zip_flags_t flags)
(define lzip/dir-add
  (let ((f (lzip-func int64 "zip_dir_add" (list '* '* zip-flags-t))))
    (lambda (archive* name flags)
      (f archive* (string->pointer name) flags))))

;; zip_int64_t zip_file_add(zip_t *archive, const char *name, zip_source_t *source,
;;                          zip_flags_t flags)
(define (lzip/file-add archive name source flags)
  (let ((f (lzip-func int64 "zip_file_add" (list '* '* '* zip-flags-t))))
    (f archive (string->pointer name) source (zip-file-flag-or flags))))

;; int zip_file_replace(zip_t *archive, zip_uint64_t index, zip_source_t *source,
;;                      zip_flags_t flags)
(define lzip/file-replace
  (lzip-func int "zip_file_replace" (list '* uint64 '* zip-flags-t)))

;; const zip_uint8_t * zip_file_extra_field_get(zip_t *archive, zip_uint64_t index,
;;                                              zip_uint16_t extra_field_index,
;;                                              zip_uint16_t *idp, zip_uint16_t *lenp,
;;                                              zip_flags_t flags)
(define lzip/file-extra-field-get
  (lzip-func uint8 "zip_file_extra_field_get" (list '* uint64 uint16 '* '*
						    zip-flags-t)))

;; const zip_uint8_t * zip_file_extra_field_get_by_id(zip_t *archive,
;;                                                    zip_uint64_t index,
;;                                                    zip_uint16_t extra_field_id,
;;                                                    zip_uint16_t extra_field_index,
;;                                                    zip_uint16_t *lenp,
;;                                                    zip_flags_t flags)
(define lzip/file-extra-field-get-by-id
  (lzip-func uint8 "zip_file_extra_field_get_by_id"
	     (list '* uint64 uint16 uint16 '* zip-flags-t)))

;; int zip_file_extra_field_delete(zip_t *archive, zip_uint64_t index,
;;                                 zip_uint16_t extra_field_index, zip_flags_t flags)
(define lzip/file-extra-field-delete
  (lzip-func int "zip_file_extra_field_delete" (list '* uint64 uint16 zip-flags-t)))

;; int zip_file_extra_field_delete_by_id(zip_t *archive, zip_uint64_t index,
;;                                       zip_uint16_t extra_field_id,
;;                                       zip_uint16_t extra_field_index,
;;                                       zip_flags_t flags)
(define lzip/file-extra-field-delete-by-id
  (lzip-func int "zip_file_extra_field_delete_by_id"
	     (list '* uint64 uint16 uint16 zip-flags-t)))

;; int zip_file_extra_field_set(zip_t *archive, zip_uint64_t index,
;;                              zip_uint16_t extra_field_id,
;;                              zip_uint16_t extra_field_index,
;;                              const zip_uint8_t *extra_field_data, zip_uint16_t len,
;;                              zip_flags_t flags)
(define lzip/file-extra-field-set
  (lzip-func int "zip_file_extra_field_set"
	     (list '* uint64 uint16 uint16 '* uint16 zip-flags-t)))

;; zip_int16_t zip_file_extra_fields_count(zip_t *archive, zip_uint64_t index,
;;                                         zip_flags_t flags)
(define lzip/file-extra-fields-count
  (lzip-func int "zip_file_extra_fields_count" (list '* uint64 zip-flags-t)))

;; zip_int16_t zip_file_extra_fields_count_by_id(zip_t *archive, zip_uint64_t index,
;;                                               zip_uint16_t extra_field_id,
;;                                               zip_flags_t flags)
(define lzip/file-extra-fields-count-by-id
  (lzip-func int16 "zip_file_extra_fields_count_by_id" (list '* uint64 uint16 zip-flags-t)))

;;; zip_source functions

;; zip_source_t * zip_source_buffer(zip_t *archive, const void *data, zip_uint64_t len,
;;                                  int freep)
(define lzip/source-buffer
  (lzip-func '* "zip_source_buffer" (list '* '* uint64 int)))

;; zip_source_t * zip_source_buffer_create(const void *data, zip_uint64_t len, int freep,
;;                                         zip_error_t *error)
(define lzip/source-buffer-create
  (lzip-func '* "zip_source_buffer_create" (list '* uint64 int '*)))

;; int zip_source_open(zip_source_t *source)
(define lzip/source-open
  (lzip-func int "zip_source_open" (list '*)))

;; int zip_source_close(zip_source_t *source)
(define lzip/source-close
  (lzip-func int "zip_source_close" (list '*)))

;; zip_int64_t zip_source_read(zip_source_t *source, void *data, zip_uint64_t len)
(define lzip/source-read
  (lzip-func int64 "zip_source_read" (list '* '* int64)))

;; zip_error_t * zip_source_error(zip_source_t *source)
(define lzip/source-error
  (let ((proc (lzip-func '* "zip_source_error" (list '*))))
    (lambda (source)
      (let ((err (proc source)))
	(zip-error err)))))

;; int zip_source_stat(zip_source_t *source, zip_stat_t *sb);
(define lzip/source-stat
  (lzip-func int "zip_source_stat" (list '* '*)))

;;; zip_t error functions

;; const char * zip_strerror(zip_t *archive)
(define lzip/strerror
  (let ((f (lzip-func '* "zip_strerror" (list '*))))
    (lambda (zip-t-archive)
      (pointer->maybe-string (f zip-t-archive)))))

;; const char * zip_file_strerror(zip_file_t *file)
(define lzip/file-strerror
  (let ((f (lzip-func '* "zip_file_strerror" (list '*))))
    (lambda (file-t*)
      (pointer->maybe-string (f file-t*)))))

;; zip_error_t * zip_get_error(zip_t *archive);
(define lzip/get-error
  (let ((f (lzip-func '* "zip_get_error" (list '*))))
    (lambda (zip-t-archive)
      (zip-error (f zip-t-archive)))))

;; const char * zip_error_strerror(zip_error_t *ze)
(define lzip/error-strerror
  (let ((f (lzip-func '* "zip_error_strerror" (list '*))))
    (lambda (zip-error)
      (pointer->maybe-string (f zip-error)))))
