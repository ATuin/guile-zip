(define-module (zip lzip)
  #:use-module (zip ffi lzip)
  #:use-module (zip utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:use-module (system foreign)
  #:export (;; archive functions
	    archive?
	    archive-file-path
	    archive-zip
	    with-archive
	    archive-open
	    archive-opened?
	    archive-close
	    archive-closed?
	    archive-files
	    archive-file
	    archive-file?
	    archive-file-names
	    archive-comment
	    archive-comment!
	    archive-password!
	    archive-add-source!
	    ;; file functions
	    file-name
	    file-archive
	    file-stats
	    file-input-port
	    with-file-input-port
	    ;; source functions
	    source?
	    bytevector->source
	    zip-file->source
	    source-len
	    source-bv))

(define-record-type <source>
  (make-source ptr len bv)
  source?
  (ptr source-ptr)
  (len source-len)
  (bv source-bv))

(define-record-type <file>
  (make-file name archive stats)
  file?
  (name file-name)
  (archive file-archive)
  (stats file-stats))

(define archive-zip-fieldn 1)
(define-record-type <archive>
  (make-archive file-path zip sources)
  archive?
  (file-path archive-file-path)
  (zip archive-zip)
  (sources archive-sources))

(set-record-type-printer! <archive>
  (lambda (archive port)
    (format port "#<<archive> file-path: ~a opened?: ~a>"
	    (archive-file-path archive) (archive-opened? archive))))

(define* (archive-open file-path #:key flags)
  "Open a zip archive"
  (let* ((err (make-int32))
	 (zf (lzip/open file-path (or flags '()) (err '&))))
    (unless (zero? (err))
      (error (format #f "Unable to open file: '~a', error: ~a" file-path (err))))
    (make-archive file-path zf (make-hash-table))))

(define* (archive-close archive #:key discard)
  "Close a zip archive"
  (cond
   ((archive-zip archive)
    (if discard
	(lzip/discard (archive-zip archive))
	(lzip/close (archive-zip archive)))
    (struct-set! archive archive-zip-fieldn #f)
    #t)
   (#t #f)))

(define (archive-opened? archive)
  "Predicate to know if archive is open"
  (let ((zip (archive-zip archive)))
    (and zip (pointer? zip))))

(define-syntax with-archive
  (syntax-rules ()
    ((with-archive (file archive-name flags) exp exp* ...)
     (let ((archive-name (archive-open file #:flags flags))) exp exp* ...
	  (when (archive-opened? archive-name)
	    (archive-close archive-name))))
    ((with-archive (file archive-name) exp exp* ...)
     (with-archive (file archive-name ()) exp exp* ...))))

(define (archive-closed? archive)
  "Predicate to know if archive is closed"
  (not (archive-opened? archive)))

(define* (archive-files archive #:key (flags '()))
  "Return list of files inside archive. Each file is a <file> record."
  (let* ((zip (archive-zip archive))
	 (n (lzip/get-num-entries zip '()))
	 (zip-stat* (make-zip-stat)))
    (map (lambda (n)
	   (lzip/stat-index zip n flags (zip-stat* '&))
	   (make-file (lzip/get-name zip n flags) archive (zip-stat*)))
	 (iota n 0))))

(define* (archive-file archive fname #:key (flags '()))
  "Return <file> with name fname in archive"
  (let ((stat* (make-zip-stat))
	(zip (archive-zip archive)))
    (if (zero? (lzip/stat zip fname flags (stat* '&)))
	(make-file (lzip/get-name zip (zip-stat-index (stat*)) flags) archive (stat*))
	#f)))

(define* (archive-file? archive fname #:key (flags '()))
  "Predicate to check if file fname is inside archive"
  (> (lzip/name-locate (archive-zip archive) fname flags) 0))

(define (archive-file-names archive)
  "Returns a list with the file names included in archive"
  (map file-name (archive-files archive)))

(define (archive-comment archive)
  "Get archive comment"
  (lzip/get-archive-comment (archive-zip archive) %null-pointer 0))

(define (archive-comment! archive comment)
  "Set archive comment"
  (lzip/set-archive-comment (archive-zip archive) comment))

(define (archive-password! archive password)
  "Set archive password"
  (lzip/set-default-password (archive-zip archive) password))

;; TODO: Check for errors here
(define* (archive-add-source! archive path source #:key (flags '())
			      (cm-flags 'CM-LEVEL-DEFAULT) cm)
  "Add source into archive in with path"
  (let ((zip (archive-zip archive))
	(dir (drop-right (string-split path #\/) 1)))
    (let ((idx (lzip/file-add zip path (source-ptr source) flags)))
      (when cm
	(lzip/file-set-compression zip idx cm cm-flags))
      (hash-set! (archive-sources archive) path source)
      idx)))

(define (file-input-port file)
  "Return a port (binary) for reading file contents"
  (let* ((stats (file-stats file))
	 (index (zip-stat-index stats))
	 (comp-method? (> (zip-stat-comp-method stats) 0))
	 (zip-t* (archive-zip (file-archive file)))
	 (file-t* (lzip/fopen-index zip-t* index 0)))
    (define (file-read! bv start count)
      (lzip/fread file-t* (bytevector->pointer bv start) count))
    (define (file-get-position)
      (lzip/ftell file-t*))
    (define (file-set-position! new-position)
      (lzip/fseek file-t* 0 new-position))
    (define (file-close)
      (lzip/fclose file-t*))
    (make-custom-binary-input-port "zip-file-port" file-read!
				   (and comp-method? file-get-position)
				   (and comp-method? file-set-position!) file-close)))

(define-syntax with-file-input-port
  (syntax-rules ()
    ((with-file-input-port (p file) exp exp* ...)
     (let ((p (file-input-port file)))
       (let ((r (begin exp exp* ...)))
	 (close-port p)
	 r)))))

(define* (bytevector->source bv #:key len (offset 0))
  "Create a source from bytevector.
The length of the bytevector can be passed otherwise it will be computed by the \
function. 
When offset is specified the bytevector will be used from offset.
When offset is specified but not len the length for the bytevector will be computed \
and the offset will be substracted. 
If len is passed by the user it will be used so the user must make sure that it \
takes in consideration the offset."
  (let* ((len (or len (- (bytevector-length bv) offset)))
	 (zip-err (zip-error))
	 (source-ptr (lzip/source-buffer-create (bytevector->pointer bv offset) len 0
						(zip-err '&))))
    (if (null-pointer? source-ptr)
	(throw 'lzip/source-buffer-create zip-err)
	(make-source source-ptr len bv))))

(define* (zip-file->source file)
  "Create a source from a zip file"
  (let ((port (file-input-port file))
	(data (get-bytevector-all (file-input-port file))))
    (close-port port)
    (bytevector->source data)))
