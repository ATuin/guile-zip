(use-modules (zip lzip)
	     (zip utils)
	     (ice-9 match))

(define (main args)
  (match-let (((_ file) args))
    (let* ((err (make-int32))
	   (zf (zip/open file 0 (err '&)))
	   (n (zip/get-num-entries zf 0))
	   (stat (make-zip-stat)))
      (format #t "~a [~a files]:~%" file n)
      (let ls ((i 0))
	(cond
	 ((< i n)
	  (zip/stat-index zf i 8 (stat '&))
	  (format #t " - ~a~%" (zip-stat-name (stat)))
	  (ls (1+ i)))
	 (else
	  (zip/close zf)))))))
